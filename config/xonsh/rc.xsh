$PATH = $PATH + ['/var/home/' + $USER + '/.cargo/bin/', '/var/home/' + $USER + '/.local/bin/']

# XONSH WIZARD START
xontrib load abbrevs argcomplete jedi mpl prompt_ret_code prompt_starship ssh_agent whole_word_jumping vox
# XONSH WIZARD END

#aliases |= {'ls': 'lsd -l --group-dirs first', 'la': 'lsd -lA --group-dirs first', 'lt': 'lsd -l --tree --depth 2 --group-dirs first'}
#aliases |= {'ls': 'exa --long --group-directories-first --git --header --icons --classify', 'la': 'exa --long --group-directories-first --git --header --icons --classify --all', 'lt': 'exa --long --group-directories-first --git --header --icons --classify --tree --level 2'}
#aliases |= {'tldr': 'toolbox run --container fedora-toolbox-custom tldr'}
#aliases |= {'pip': 'toolbox run pip'}
#aliases |= {'lsrc': 'toolbox run --container fedora-toolbox-custom lsrc', 'mkrc': 'toolbox run --container fedora-toolbox-custom mkrc', 'rcdn': 'toolbox run --container fedora-toolbox-custom rcdn', 'rcup': 'toolbox run --container fedora-toolbox-custom rcup'}
aliases |= {'..': 'cd ..', '...': 'cd ../..', '....': 'cd ../../..'}
aliases |= {'find': 'fd'}

$VISUAL = 'nvim'
$EDITOR = 'nvim'
$CASE_SENSITIVE_COMPLETIONS = False

execx($(starship init xonsh))
