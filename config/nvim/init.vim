" Everything inside this if statement is ignored if NeoVim is run inside VS Code
if !exists('g:vscode')

  " ###############################
  " ###         PLUGINS         ###
  " ###############################

  " Autoinstall vim-plug
  " Docs: https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
  if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif

  " Specify a directory for plugins
  " - For Neovim: stdpath('data') . '/plugged'
  " - Avoid using standard Vim directory names like 'plugin'
  call plug#begin(stdpath('data') . '/plugged')

  " Make sure you use single quotes

  " Shorthand notation; fetches https://github.com/preservim/nerdtree     
  Plug 'preservim/nerdtree' " Tree-folder-view - Command `:NERDTree` 
  Plug 'scrooloose/syntastic' " Language pack
  Plug 'Shougo/deoplete.nvim' " Autocompletion - Needs `msgpack` from pip
  Plug 'Xuyuanp/nerdtree-git-plugin' " Git icons for NERDTree
  Plug 'tpope/vim-fugitive' " Git plugin - Usage `:Git`/`:G`
  Plug 'airblade/vim-gitgutter' " git diff in sign column (line number column)
  Plug 'vim-airline/vim-airline' " Powerline-like statusline
  Plug 'vim-airline/vim-airline-themes' " vim-airline themes - Usage: `:AirlineTheme <theme>` eg. `:AirlineTheme murmur`

  " Initialize plugin system
  call plug#end()

  " ###############################
  " ###        PLUGINS END      ###
  " ###############################

  " Setting default airline theme
  let g:airline_theme='powerlineish'
  let g:airline_powerline_fonts = 1

  set number            " Enable line numbers in the left margin
  set cursorline        " highlight current cursorline

  " Show whitespaces
  set listchars=tab:>-,trail:~,extends:>,precedes:<
  set list

  " Tabs
  set expandtab               " Spaces instead of TAB
  set tabstop=2               " number of columns occupied by a tab 
  set softtabstop=2           " see multiple spaces as tabstops so 
  set shiftround              " round indentation to multiples of 'shiftwidth' when shifting text
  set autoindent              " reproduce the indentation of the previous line
  set shiftwidth=2            " Width for autoindents
  filetype plugin indent on " use language‐specific plugins for indenting

  " Search
  set hlsearch                " highlight search 
  set incsearch               " incremental search

  " Mouse
  set mouse=a                 " enable mouse click
  set cursorline              " highlight current cursorline

  " NerdTree Sidepanel
  autocmd VimEnter * NERDTree
  autocmd BufEnter * NERDTreeMirror
  autocmd VimEnter * NERDTreeFind " Jump to current directory

  " Close NERDTree if the file windows closes
  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") 
      \ && b:NERDTree.isTabTree()) | q | endif

  " Terminal Panel
  autocmd TermOpen * setlocal nonumber norelativenumber " Don't show line numbers in terminal
  autocmd TermOpen * startinsert
  "  autocmd BufWinEnter,WinEnter term://* startinsert " Go to insert mode when entering terminal window
  autocmd BufLeave term://* stopinsert " Return to normal mode when leaving a terminal

  " Toggle 'default' terminal
  nnoremap <C-t> :call ChooseTerm("term-slider", 1)<CR>
  " Start terminal in current pane
  "nnoremap <C-p> :call ChooseTerm("term-pane", 0)<CR>

  function! ChooseTerm(termname, slider) " Pop-up terminal
    let pane = bufwinnr(a:termname)
    let buf = bufexists(a:termname)
    if pane > 0
      " pane is visible
      if a:slider > 0
        :exe pane . "wincmd c"
      else
        :exe "e #" 
      endif
    elseif buf > 0
      " buffer is not in pane
      if a:slider
        :exe "topleft 15split"
      endif
      :exe "buffer " . a:termname
    else
      " buffer is not loaded, create
      if a:slider
        :exe "topleft 15split"
      endif
      :terminal
      :exe "f " a:termname
    endif
  endfunction 
  autocmd VimEnter * wincmd w " Start with cursor in file editing window
endif
